package cz.myhouse.reminder.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

@ConfigurationProperties(prefix = "server")
@Validated
@ConstructorBinding
@AllArgsConstructor
@ToString
public class ServerConfig {

    @Getter
    @Setter
    private String host;


    @Getter
    @Setter
    private int port;
}
