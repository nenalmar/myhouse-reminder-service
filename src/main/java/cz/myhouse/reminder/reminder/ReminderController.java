package cz.myhouse.reminder.reminder;

import cz.myhouse.reminder.document.DocumentDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@Api("Reminder")
@RequestMapping("/reminder")
@Slf4j
@RequiredArgsConstructor
public class ReminderController {

    private final ReminderService reminderService;

    @GetMapping("/expired")
    @ApiOperation("Return records")
    List<ReminderDTO> getExpired() {
        log.info("ReminderController, method getExpired: Return all expired documents");
        return reminderService.getExpiredDocuments();
    }
}
