package cz.myhouse.reminder.reminder;

import cz.myhouse.reminder.document.DocumentDto;

import java.util.List;

public interface ReminderService {

    List<ReminderDTO> getExpiredDocuments();
}
