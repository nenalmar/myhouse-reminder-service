package cz.myhouse.reminder.reminder;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class ReminderDTO {

    private Long id;
    private String documentType;
    private LocalDate expirationDate;
    private int daysToExpiration;
    private boolean isExpired;
}
