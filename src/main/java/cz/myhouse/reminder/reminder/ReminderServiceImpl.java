package cz.myhouse.reminder.reminder;

import cz.myhouse.reminder.document.DocumentDto;
import cz.myhouse.reminder.document.DocumentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReminderServiceImpl implements ReminderService{

    private final DocumentService documentService;

    @Override
    public List<ReminderDTO> getExpiredDocuments() {
        List<ReminderDTO> reminderDtoList = new ArrayList<>();
        List<DocumentDto> documentDtoList = documentService.getDocuments();

        documentDtoList.forEach(documentDto -> {
            ReminderDTO reminderDto = convertDocumentDtoToReminderDto(documentDto);
            if (reminderDto.isExpired()) {
                reminderDtoList.add(reminderDto);
            }
        });

        return reminderDtoList;
    }

    private ReminderDTO convertDocumentDtoToReminderDto(DocumentDto documentDto) {
        log.info("ReminderController, method convertDocumentDtoToReminderDto: Convert DocumentDto to ReminderDto");
        return ReminderDTO.builder()
                .id(documentDto.getDocumentId())
                .documentType(documentDto.getDocumentType())
                .expirationDate(documentDto.getExpirationDate())
                .daysToExpiration(getDaysToExpiration(documentDto.getExpirationDate()))
                .isExpired(isExpired(documentDto.getExpirationDate()))
                .build();
    }

    int getDaysToExpiration(LocalDate expirationDate) {
        int daysToExpiration = (int) ChronoUnit.DAYS.between(LocalDate.now(), expirationDate);
        if (daysToExpiration < 0) {
            daysToExpiration = 0;
        }
        return daysToExpiration;
    }
    
    boolean isExpired(LocalDate expirationDate) {
        return LocalDate.now().compareTo(expirationDate) > 0;
    }
}
