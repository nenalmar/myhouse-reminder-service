package cz.myhouse.reminder.document;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Slf4j
@Component
public class DocumentRestClientServiceImpl implements DocumentRestClientService {

    private static final String PATH_DOCUMENT_SERVICE = "http://localhost:9001/documents/all";

    private WebClient webClient() {
        return WebClient.builder()
                .baseUrl(PATH_DOCUMENT_SERVICE)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .build();
    }

    @Override
    public List<DocumentDto> findAll() {
        log.info("DocumentRestClientServiceImpl: start finding all documnets from document-service");
        DocumentDto[] documents;

        try {
            documents = webClient()
                    .get()
                    .retrieve()
                    .bodyToMono(DocumentDto[].class)
                    .block();
            log.error("DocumentRestClientServiceImpl: findAll(): Found document service.");
        } catch (WebClientException e) {
            log.error("DocumentRestClientServiceImpl: findAll(): Can not find document service.");
            return Collections.emptyList();
        }



        if (documents == null) {
            return Collections.emptyList();
        }

        log.info("DocumentRestClientServiceImpl: all documnets was found from document-service, size " + documents.length);

        return Arrays.asList(documents);
    }
}
