package cz.myhouse.reminder.document;

import java.util.List;

public interface DocumentRestClientService {

    List<DocumentDto> findAll();
}
