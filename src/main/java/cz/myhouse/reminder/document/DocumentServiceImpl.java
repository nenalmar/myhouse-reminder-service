package cz.myhouse.reminder.document;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class DocumentServiceImpl implements DocumentService {

    private final DocumentRestClientService documentRestClientService;
    private final DocumentRepository documentRepository;

    @Override
    public void saveDocument(DocumentDto documentDto) throws ParseException {
        Document document = convertDocumentDtoToDocument(documentDto);
        documentRepository.save(document);
    }

    @Override
    @Scheduled(initialDelay = 3000, fixedDelay = 3000)
    public void checkNewDocuments() {
        log.info("DocumentServiceImpl: Check new documnets from document-service");
        List<DocumentDto> documents = documentRestClientService.findAll();

        documents.forEach(documentDto -> {
            try {
                if (isNotDocumentStored(documentDto)) {
                    saveDocument(documentDto);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        log.info("DocumentServiceImpl: " + documents.size() + " was new and stored.");
    }

    @Override
    public List<DocumentDto> getDocuments() {
        List<DocumentDto> documentDtoList = new ArrayList<>();
        List<Document> documents = documentRepository.findAll();

        documents.forEach(document -> {
            documentDtoList.add(DocumentDto.builder()
                    .documentId(document.getId())
                    .documentType(document.getDocumentType())
                    .expirationDate(convertDateToLocalDate(document.getExpirationDate()))
                    .build());
        });

        return documentDtoList;
    }

    private LocalDate convertDateToLocalDate(Date date) {
        LocalDateTime localDateTime = ((Timestamp) date).toLocalDateTime();

        return localDateTime.toLocalDate();
    }

    private Document convertDocumentDtoToDocument(DocumentDto documentDto) throws ParseException {
        log.info("DocumentServiceImpl: Convert documentDto to Document");
        Date expirationDate = new SimpleDateFormat("yyyy-MM-dd").parse(documentDto.getExpirationDate().toString());

        Document document = new Document();
        document.setDocumentType(documentDto.getDocumentType());
        document.setExpirationDate(expirationDate);

        return document;
    }

    private boolean isNotDocumentStored(DocumentDto documentDto) {
        return !documentRepository.existsById(documentDto.getDocumentId());
    }
}
