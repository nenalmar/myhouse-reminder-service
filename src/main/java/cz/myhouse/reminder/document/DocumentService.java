package cz.myhouse.reminder.document;

import java.text.ParseException;
import java.util.List;

public interface DocumentService {

    void saveDocument(DocumentDto documentDto) throws ParseException;
    void checkNewDocuments();
    List<DocumentDto> getDocuments();
}
