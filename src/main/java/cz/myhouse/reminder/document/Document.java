package cz.myhouse.reminder.document;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "document_type")
    private String documentType;
    @Column(name = "expiration_date")
    private Date expirationDate;
}
